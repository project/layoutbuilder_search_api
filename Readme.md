CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

The Layout Builder Search API allows you to index fields from blocks used by Layout Builder. This is useful, if you don't want to index the rendered page.
Instead, you can selectively add fields from related blocks.

* For a full description of the module visit:
  https://www.drupal.org/project/layoutbuilder_search_api

* To submit bug reports and feature suggestions, or to track changes visit:
  https://www.drupal.org/project/issues/layoutbuilder_search_api


REQUIREMENTS
------------

This module requires the [Search API](https://www.drupal.org/project/search_api) and the Layout Builder form core.


INSTALLATION
------------

* Install the Layout Builder Search API module as you would normally install a
  contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
  further information.


CONFIGURATION
-------------

* Go to a Search API index > Select tab "Processors" > Enable "Layout builder references"

* Now you can enable all supported "Block Content Types" in the tab below.

* Go to tab "Fields" and add the newly awailable fields from the enabled "Block Content Types"


MAINTAINERS
-----------

* ayalon - https://www.drupal.org/u/ayalon

Supporting organization:

* Liip - https://www.drupal.org/liip
